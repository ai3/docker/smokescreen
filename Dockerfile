FROM golang:1.20 AS build

WORKDIR /tmp
RUN go install -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo github.com/stripe/smokescreen@latest && ls -l /go/bin/smokescreen

FROM scratch
COPY --from=build /go/bin/smokescreen /smokescreen

ENTRYPOINT ["/smokescreen"]

